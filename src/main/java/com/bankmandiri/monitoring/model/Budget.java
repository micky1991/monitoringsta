package com.bankmandiri.monitoring.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="budget")
public class Budget {

	
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	@Column(name="IdBudget")
//	private String IdBudget;
	
	@EmbeddedId
	BudgetId budgetId;
	
	@Column(name="BudgetAmount")
	private long BudgetAmount;

//	public String getIdBudget() {
//		return IdBudget;
//	}
//
//	public void setIdBudget(String IdBudget) {
//		this.IdBudget = IdBudget;
//	}

	public BudgetId getBudgetId() {
		return budgetId;
	}

	public void setBudgetId(String ProjectCode, int Tahun) {
		this.budgetId = new BudgetId(ProjectCode, Tahun);
	}

	public long getBudgetAmount() {
		return BudgetAmount;
	}

	public void setBudgetAmount(long budgetAmount) {
		BudgetAmount = budgetAmount;
	}
}
