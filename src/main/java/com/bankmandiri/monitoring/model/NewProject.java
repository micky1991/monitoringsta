package com.bankmandiri.monitoring.model;

public class NewProject {
	String ProjectCode;
	String ProjectName;
	int Tahun;
	long Budget;
	String IdPM;
	
	public String getProjectCode() {
		return ProjectCode;
	}
	public void setProjectCode(String projectCode) {
		ProjectCode = projectCode;
	}
	public String getProjectName() {
		return ProjectName;
	}
	public void setProjectName(String projectName) {
		ProjectName = projectName;
	}
	public int getTahun() {
		return Tahun;
	}
	public void setTahun(int tahun) {
		Tahun = tahun;
	}
	public long getBudget() {
		return Budget;
	}
	public void setBudget(long budget) {
		Budget = budget;
	}
	public String getIdPM() {
		return IdPM;
	}
	public void setIdPM(String idPM) {
		IdPM = idPM;
	}
}
