package com.bankmandiri.monitoring.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class ProjectManagerId implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1220756038127833994L;
	@NotNull
	String IdPegawai;
	@NotNull
	String ProjectCode;
	
	public ProjectManagerId() {
		
	}
	
	public ProjectManagerId(String IdPegawai, String ProjectCode) {
		this.IdPegawai = IdPegawai;
		this.ProjectCode = ProjectCode;
	}

	public String getIdPegawai() {
		return IdPegawai;
	}
	public void setIdPegawai(String idPegawai) {
		IdPegawai = idPegawai;
	}
	public String getProjectCode() {
		return ProjectCode;
	}
	public void setProjectCode(String projectCode) {
		ProjectCode = projectCode;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectManagerId that = (ProjectManagerId) o;

        if (!IdPegawai.equals(that.IdPegawai)) return false;
        return ProjectCode.equals(that.ProjectCode);
    }

    @Override
    public int hashCode() {
        int result = IdPegawai.hashCode();
        result = 31 * result + ProjectCode.hashCode();
        return result;
    }
}
