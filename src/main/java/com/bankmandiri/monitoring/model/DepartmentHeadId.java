package com.bankmandiri.monitoring.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class DepartmentHeadId implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1591653793267276632L;
	@NotNull
	String IdPegawai;
	@NotNull
	String IdPM;
	
	public DepartmentHeadId(String IdPegawai, String IdPM) {
		this.IdPegawai = IdPegawai;
		this.IdPM = IdPM;
	}
	
	public String getIdPegawai() {
		return IdPegawai;
	}
	public void setIdPegawai(String idPegawai) {
		IdPegawai = idPegawai;
	}
	public String getIdPM() {
		return IdPM;
	}
	public void setIdPM(String idPM) {
		IdPM = idPM;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DepartmentHeadId that = (DepartmentHeadId) o;

        if (!IdPegawai.equals(that.IdPegawai)) return false;
        return IdPM.equals(that.IdPM);
    }
}
