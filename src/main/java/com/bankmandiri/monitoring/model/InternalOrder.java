package com.bankmandiri.monitoring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="InternalOrder")
public class InternalOrder {
	
	@Id
	@Column(name="IdIo")
	private String IdIO;
	
	@Column(name="ProjectCode")
	private String ProjectCode;
	
	@Column(name="NamaIo")
	private String NamaIO;

	public String getIdIO() {
		return IdIO;
	}

	public void setIdIO(String IdIO) {
		this.IdIO = IdIO;
	}
	
	public String getProjectCode() {
		return ProjectCode;
	}

	public void setProjectCode(String projectCode) {
		ProjectCode = projectCode;
	}

	public String getNamaIO() {
		return NamaIO;
	}

	public void setNamaIO(String NamaIO) {
		this.NamaIO = NamaIO;
	}
}
