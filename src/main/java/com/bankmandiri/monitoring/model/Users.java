package com.bankmandiri.monitoring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class Users {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="UserId", unique = true)
	private String UserId;
	
	@Column(name="Username", unique = true)
	private String Username;
	
	@Column(name="Password")
	private String Password;
	
	@Column(name="Role")
	private int Role;

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		this.UserId = userId;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		this.Username = username;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		this.Password = password;
	}

	public int getRole() {
		return Role;
	}

	public void setRole(int role) {
		this.Role = role;
	}
}
