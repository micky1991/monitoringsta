package com.bankmandiri.monitoring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipe_prognosa")
public class TipePrognosa {
	@Id
	@Column(name="idPrognosa")
	private String idPrognosa;
	
	@Column(name="namaPrognosa")
	private String namaPrognosa;

	public String getIdPrognosa() {
		return idPrognosa;
	}

	public void setIdPrognosa(String idPrognosa) {
		this.idPrognosa = idPrognosa;
	}

	public String getNamaPrognosa() {
		return namaPrognosa;
	}

	public void setNamaPrognosa(String namaPrognosa) {
		this.namaPrognosa = namaPrognosa;
	}
}
