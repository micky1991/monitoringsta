package com.bankmandiri.monitoring.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name="ProjectManager")
public class ProjectManager implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	ProjectManagerId projectManagerId;
	
	public ProjectManager() {
		
	}
	
	public ProjectManager(String IdPegawai, String ProjectCode) {
		this.projectManagerId = new ProjectManagerId(IdPegawai, ProjectCode);
	}

	public ProjectManagerId getProjectManagerId() {
		return projectManagerId;
	}

	public void setProjectManagerId(String IdPegawai, String ProjectCode) {
		this.projectManagerId = new ProjectManagerId(IdPegawai, ProjectCode);
	}
}
