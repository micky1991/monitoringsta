package com.bankmandiri.monitoring.model;

import javax.persistence.Column;

public class TipeAnggaran {
	@Column(name="idTipeAnggaran")
	private String idTipeAnggaran;
	
	@Column(name="namaTipeAnggaran")
	private String namaTipeAnggaran;

	public String getIdTipeAnggaran() {
		return idTipeAnggaran;
	}

	public void setIdTipeAnggaran(String idTipeAnggaran) {
		this.idTipeAnggaran = idTipeAnggaran;
	}

	public String getNamaTipeAnggaran() {
		return namaTipeAnggaran;
	}

	public void setNamaTipeAnggaran(String namaTipeAnggaran) {
		this.namaTipeAnggaran = namaTipeAnggaran;
	}
}
