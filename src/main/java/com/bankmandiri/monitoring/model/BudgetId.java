package com.bankmandiri.monitoring.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class BudgetId implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull
	private String ProjectCode;
	
	@NotNull
	private int Tahun;
	
	public BudgetId() {
		
	}
	
	public BudgetId(String ProjectCode, int Tahun) {
		this.ProjectCode = ProjectCode;
		this.Tahun = Tahun;
	}

	public String getProjectCode() {
		return ProjectCode;
	}

	public void setProjectCode(String projectCode) {
		ProjectCode = projectCode;
	}

	public int getTahun() {
		return Tahun;
	}

	public void setTahun(int tahun) {
		Tahun = tahun;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BudgetId that = (BudgetId) o;
        that.Tahun = 0;
        if (!(Tahun == that.Tahun)) return false;
        return ProjectCode.equals(that.ProjectCode);
    }

    @Override
    public int hashCode() {
        int result = Tahun;
        result = 31 * result + ProjectCode.hashCode();
        return result;
    }
}
