package com.bankmandiri.monitoring.model;

public class Response {
	private String flag;
	private String info;
	private String data;
	
	public Response(String flag, String info, String data) {
		this.flag = flag;
		this.info = info;
		this.data = data;
	}
	
	public Response() {
		
	}
	
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
