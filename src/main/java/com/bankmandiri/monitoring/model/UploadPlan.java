package com.bankmandiri.monitoring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UploadPlan")
public class UploadPlan {
	@Id
	@Column(name="IdPengadaan")
	private String idPengadaan;
	
	@Column(name="Quartal")
	private int Quartal;
	
	@Column(name="Tahun")
	private int Tahun;
	
	@Column(name="Nominal")
	private Long Nominal;

	public String getIdPengadaan() {
		return idPengadaan;
	}

	public void setIdPengadaan(String idPengadaan) {
		this.idPengadaan = idPengadaan;
	}

	public int getQuartal() {
		return Quartal;
	}

	public void setQuartal(int quartal) {
		Quartal = quartal;
	}

	public int getTahun() {
		return Tahun;
	}

	public void setTahun(int tahun) {
		Tahun = tahun;
	}

	public Long getNominal() {
		return Nominal;
	}

	public void setNominal(Long nominal) {
		Nominal = nominal;
	}
}
