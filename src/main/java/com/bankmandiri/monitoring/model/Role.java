package com.bankmandiri.monitoring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "role")
public class Role {
	@Id
	@Column(name="IdRole")
	private String IdRole;
	
	@Column(name="NamaRole")
	private String NamaRole;

	public String getIdRole() {
		return IdRole;
	}

	public void setIdRole(String idRole) {
		IdRole = idRole;
	}

	public String getNamaRole() {
		return NamaRole;
	}

	public void setNamaRole(String namaRole) {
		NamaRole = namaRole;
	}
}
