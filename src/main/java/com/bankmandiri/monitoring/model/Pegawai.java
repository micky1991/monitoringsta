package com.bankmandiri.monitoring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Pegawai")
public class Pegawai {
	
	@Id
	@Column
	String IdPegawai;
	
	@Column
	String NamaPegawai;
	
	@Column
	String ContactNumber;
	
	@Column
	String EmailAddress;
	
	@Column
	int Status;

	public String getIdPegawai() {
		return IdPegawai;
	}

	public void setIdPegawai(String idPegawai) {
		IdPegawai = idPegawai;
	}

	public String getNamaPegawai() {
		return NamaPegawai;
	}

	public void setNamaPegawai(String namaPegawai) {
		NamaPegawai = namaPegawai;
	}

	public String getContactNumber() {
		return ContactNumber;
	}

	public void setContactNumber(String contactNumber) {
		ContactNumber = contactNumber;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public int getStatus() {
		return Status;
	}

	public void setStatus(int status) {
		Status = status;
	}
}
