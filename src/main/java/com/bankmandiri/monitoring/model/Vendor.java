package com.bankmandiri.monitoring.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name="vendor")
public class Vendor{
	@Id	
	@Column(name="IdVendor")
	private String IdVendor;
	
	@Column(name="NamaVendor")
	private String NamaVendor;
	
	@Column(name="Deskripsi")
	private String Deskripsi;

	public String getIdVendor() {
		return IdVendor;
	}

	public void setIdVendor(String IdVendor) {
		this.IdVendor = IdVendor;
	}

	public String getNamaVendor() {
		return NamaVendor;
	}

	public void setNamaVendor(String NamaVendor) {
		this.NamaVendor = NamaVendor;
	}

	public String getDeskripsi() {
		return Deskripsi;
	}

	public void setDeskripsi(String Deskripsi) {
		this.Deskripsi = Deskripsi;
	}
}
