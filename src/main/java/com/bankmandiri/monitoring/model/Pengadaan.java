package com.bankmandiri.monitoring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Pengadaan")
public class Pengadaan {
	
	@Id
	@Column(name="NoId")
	private String NoId;
	
	@Column(name="ProjectCode")
	private String ProjectCode;
	
	@Column(name="DeskripsiPengadaan")
	private String DeskripsiPengadaan;
	
	@Column(name="DetailPengadaan")
	private String DetailPengadaan;
	
	@Column(name="IdIO")
	private String IdIO;
	
	@Column(name="IdVendor")
	private String IdVendor;
	
	@Column(name="NilaiPlan")
	private long NilaiPlan;
	
	@Column(name="NilaiRekomendasi")
	private long NilaiRekomendasi;
	
	@Column(name="NilaiKontrak")
	private long NilaiKontrak;

	@Column(name="Asset")
	private String Asset;
	
	@Column(name="Tahun")
	private int Tahun;
	
	@Column(name="FormPermintaan")
	private String FormPermintaan;
	
	@Column(name="FormRekomendasi")
	private String FormRekomendasi;
	
	@Column(name="NotaRekomendasiAnggaran")
	private String NotaRekomendasiAnggaran;
	
	@Column(name="FS")
	private int FS;
	
	@Column(name="BRD")
	private int BRD;
	
	@Column(name="PC")
	private int PC;
	
	@Column(name="MasterAsset")
	private String MasterAsset;
	
	public String getProjectCode() {
		return ProjectCode;
	}

	public void setProjectCode(String ProjectCode) {
		this.ProjectCode = ProjectCode;
	}

	public String getDeskripsiPengadaan() {
		return DeskripsiPengadaan;
	}

	public void setDeskripsiPengadaan(String DeskripsiPengadaan) {
		this.DeskripsiPengadaan = DeskripsiPengadaan;
	}

	public String getIdIO() {
		return IdIO;
	}

	public void setIdIO(String IdIO) {
		this.IdIO = IdIO;
	}

	public String getIdVendor() {
		return IdVendor;
	}

	public void setIdVendor(String IdVendor) {
		this.IdVendor = IdVendor;
	}

	public long getNilaiPlan() {
		return NilaiPlan;
	}

	public void setNilaiPlan(long NilaiPlan) {
		this.NilaiPlan = NilaiPlan;
	}

	public long getNilaiKontrak() {
		return NilaiKontrak;
	}

	public void setNilaiKontrak(long NilaiKontrak) {
		this.NilaiKontrak = NilaiKontrak;
	}

	public String getAsset() {
		return Asset;
	}

	public void setAsset(String Asset) {
		this.Asset = Asset;
	}

	public String getNoId() {
		return NoId;
	}

	public void setNoId(String noId) {
		NoId = noId;
	}

	public String getFormPermintaan() {
		return FormPermintaan;
	}

	public void setFormPermintaan(String formPermintaan) {
		FormPermintaan = formPermintaan;
	}

	public String getFormRekomendasi() {
		return FormRekomendasi;
	}

	public void setFormRekomendasi(String formRekomendasi) {
		FormRekomendasi = formRekomendasi;
	}

	public String getNotaRekomendasiAnggaran() {
		return NotaRekomendasiAnggaran;
	}

	public void setNotaRekomendasiAnggaran(String notaRekomendasiAnggaran) {
		NotaRekomendasiAnggaran = notaRekomendasiAnggaran;
	}

	public String getDetailPengadaan() {
		return DetailPengadaan;
	}

	public void setDetailPengadaan(String detailPengadaan) {
		DetailPengadaan = detailPengadaan;
	}

	public long getNilaiRekomendasi() {
		return NilaiRekomendasi;
	}

	public void setNilaiRekomendasi(long nilaiRekomendasi) {
		NilaiRekomendasi = nilaiRekomendasi;
	}

	public int getTahun() {
		return Tahun;
	}

	public void setTahun(int tahun) {
		Tahun = tahun;
	}

	public int getFS() {
		return FS;
	}

	public void setFS(int fS) {
		FS = fS;
	}

	public int getBRD() {
		return BRD;
	}

	public void setBRD(int bRD) {
		BRD = bRD;
	}

	public int getPC() {
		return PC;
	}

	public void setPC(int pC) {
		PC = pC;
	}

	public String getMasterAsset() {
		return MasterAsset;
	}

	public void setMasterAsset(String masterAsset) {
		MasterAsset = masterAsset;
	}
}
