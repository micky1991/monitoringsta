package com.bankmandiri.monitoring.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table (name = "DepartmentHead")
public class DepartmentHead implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	DepartmentHeadId departmentHeadId;

	public DepartmentHeadId getDepartmentHeadId() {
		return departmentHeadId;
	}

	public void setDepartmentHeadId(String IdPegawai, String IdPM) {
		this.departmentHeadId = new DepartmentHeadId(IdPegawai, IdPM);
	}
}
