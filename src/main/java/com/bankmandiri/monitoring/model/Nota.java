package com.bankmandiri.monitoring.model;

import javax.persistence.Column;

public class Nota {
	@Column(name="idPengadaan")
	private String id_pengadaan;
	
	@Column(name="formRekomendasi")
	private String form_rekomendasi;
	
	@Column(name="notaPermintaan")
	private String nota_permintaan;
	
	@Column(name="notaRekomendasiAnggaran")
	private String nota_rekomendasi_anggaran;

	public String getId_pengadaan() {
		return id_pengadaan;
	}

	public void setId_pengadaan(String id_pengadaan) {
		this.id_pengadaan = id_pengadaan;
	}

	public String getForm_rekomendasi() {
		return form_rekomendasi;
	}

	public void setForm_rekomendasi(String form_rekomendasi) {
		this.form_rekomendasi = form_rekomendasi;
	}

	public String getNota_permintaan() {
		return nota_permintaan;
	}

	public void setNota_permintaan(String nota_permintaan) {
		this.nota_permintaan = nota_permintaan;
	}

	public String getNota_rekomendasi_anggaran() {
		return nota_rekomendasi_anggaran;
	}

	public void setNota_rekomendasi_anggaran(String nota_rekomendasi_anggaran) {
		this.nota_rekomendasi_anggaran = nota_rekomendasi_anggaran;
	}
}
