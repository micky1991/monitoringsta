package com.bankmandiri.monitoring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bankmandiri.monitoring.model.Budget;
import com.bankmandiri.monitoring.model.DepartmentHead;
import com.bankmandiri.monitoring.model.InternalOrder;
import com.bankmandiri.monitoring.model.Pegawai;
import com.bankmandiri.monitoring.model.Pengadaan;
import com.bankmandiri.monitoring.model.Project;
import com.bankmandiri.monitoring.model.ProjectManager;
import com.bankmandiri.monitoring.model.Users;
import com.bankmandiri.monitoring.model.Vendor;
import com.bankmandiri.monitoring.repository.BudgetRepository;
import com.bankmandiri.monitoring.repository.DepartmentHeadRepository;
import com.bankmandiri.monitoring.repository.InternalOrderRepository;
import com.bankmandiri.monitoring.repository.PegawaiRepository;
import com.bankmandiri.monitoring.repository.PengadaanRepository;
import com.bankmandiri.monitoring.repository.ProjectManagerRepository;
import com.bankmandiri.monitoring.repository.ProjectRepository;
import com.bankmandiri.monitoring.repository.TipePrognosaRepository;
import com.bankmandiri.monitoring.repository.UserRepository;
import com.bankmandiri.monitoring.repository.VendorRepository;
import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
public class UploadController{
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	InternalOrderRepository internalOrderRepository;
	
	@Autowired
	VendorRepository vendorRepository;
	
	@Autowired
	TipePrognosaRepository tipePrognosaRepository;
	
	@Autowired
	PengadaanRepository pengadaanRepository;
	
	@Autowired
	PegawaiRepository pegawaiRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ProjectManagerRepository projectManagerRepository;
	
	@Autowired
	DepartmentHeadRepository departmentHeadRepository;

	@Autowired
	BudgetRepository budgetRepository;
	
//    @GetMapping("/")
//    public String index() {
//        return "upload";
//    }

    @RequestMapping("/upload") // //new annotation since 4.3
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }

        try {
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            
            File convFile = new File(file.getOriginalFilename());
			convFile.createNewFile();
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(bytes);
			fos.close();
			
			String[] line;
			Integer lineNumber = 0;
			
			if(file.getOriginalFilename().equals("pengadaan.csv")) {
				try(CSVReader reader = new CSVReader(new FileReader(convFile))){
					reader.readNext(); //skip column
					while ((line = reader.readNext()) != null) {
						lineNumber++;
						
						Project project = new Project();
						project.setProjectCode(line[0]);
						project.setProjectName(line[1]);
						projectRepository.save(project);
						
						InternalOrder internalOrder = new InternalOrder();
						internalOrder.setIdIO(line[2]);
						internalOrder.setProjectCode(line[0]);
						internalOrderRepository.save(internalOrder);
						
						Vendor vendor = new Vendor();
						vendor.setIdVendor(line[11]);
						vendor.setNamaVendor(line[12]);
						vendorRepository.save(vendor);
						
						Pengadaan pengadaan = new Pengadaan();
						pengadaan.setNoId(getCurrentTime()+ line[0]);
						
						pengadaan.setProjectCode(line[0]);
						pengadaan.setIdIO(line[2]);
						pengadaan.setIdVendor(line[11]);
						pengadaan.setDeskripsiPengadaan(line[4]);
						pengadaan.setAsset(line[5]);
						pengadaan.setTahun(Integer.parseInt(line[6]));
						pengadaan.setNilaiKontrak(Long.parseLong(line[9]));
						pengadaan.setNilaiPlan(Long.parseLong(line[7]));
						pengadaan.setNilaiRekomendasi(Long.parseLong(line[8]));
						pengadaan.setDetailPengadaan(line[10]);
						pengadaan.setFormRekomendasi(line[13]);
						pengadaan.setFormPermintaan(line[14]);
						pengadaan.setNotaRekomendasiAnggaran(line[15]);
						pengadaan.setFS(Integer.parseInt(line[16]));
						pengadaan.setBRD(Integer.parseInt(line[17]));
						pengadaan.setPC(Integer.parseInt(line[18]));
						pengadaan.setMasterAsset(line[19]);
						pengadaanRepository.save(pengadaan);
						
						//line0 = project code
						//line1 = Project name
						//line2 = Io
						//line3 = Nama IO
						//line4 = Deskripsi pengadaan
						//line5 = Asset
						//line6 = Tahun Pengadaan
						//line7 = nilai plan
						//line8 = Rekomendasi anggaran
						//line9 = nilai kontrak / asumsi
						//line10 = detail pengadaan
						//line11 = id vendor
						//line12 = Nama vendor
						//line13 = form rekomendasi
						//line14 = nota permintaan
						//line15 = nota rekomendasi anggaran
						//line16 = fs
						//line17 = brd
						//line18 = pc
						//line19 = master asset
					}
				}
			} else if(file.getOriginalFilename().equals("pegawai.csv")) {
				try(CSVReader reader = new CSVReader(new FileReader(convFile))){
					reader.readNext(); //skip column
					while ((line = reader.readNext()) != null) {
						lineNumber++;
						
						Pegawai pegawai = new Pegawai();
						pegawai.setIdPegawai(line[0]);
						pegawai.setNamaPegawai(line[1]);
						pegawai.setContactNumber(line[2]);
						pegawai.setEmailAddress(line[3]);
						pegawai.setStatus(Integer.parseInt(line[4]));
						pegawaiRepository.save(pegawai);
						
						//line0 = ID Pegawai
						//line1 = Nama Pegawai
						//line2 = Contact
						//line3 = Email
						//line4 = status
					}
				}
			} else if(file.getOriginalFilename().equals("internalorder.csv")) {
				try(CSVReader reader = new CSVReader(new FileReader(convFile))){
					reader.readNext(); //skip column
					while ((line = reader.readNext()) != null) {
						lineNumber++;
						
						InternalOrder internalorder = new InternalOrder();
						internalorder.setIdIO(line[0]);
						internalorder.setProjectCode(line[1]);
						internalorder.setNamaIO(line[2]);
						internalOrderRepository.save(internalorder);
						
						//line0 = IDIO
						//line1 = Project Code
						//line2 = Nama IO
					}
				}
			} else if(file.getOriginalFilename().equals("vendor.csv")) {
				try(CSVReader reader = new CSVReader(new FileReader(convFile))){
					reader.readNext(); //skip column
					while ((line = reader.readNext()) != null) {
						lineNumber++;
						
						Vendor vendor = new Vendor();
						vendor.setIdVendor(line[0]);
						vendor.setNamaVendor(line[1]);
						vendor.setDeskripsi(line[2]);
						vendorRepository.save(vendor);
						
						//line0 = ID Vendor
						//line1 = Nama Vendor
						//line2 = Deskripsi
					}
				}
			} else if(file.getOriginalFilename().equals("project.csv")) {
				try(CSVReader reader = new CSVReader(new FileReader(convFile))){
					reader.readNext(); //skip column
					while ((line = reader.readNext()) != null) {
						lineNumber++;
						
						Project project = new Project();
						project.setProjectCode(line[0]);
						project.setProjectName(line[1]);
						projectRepository.save(project);
						
						Budget budget = new Budget();
						budget.setBudgetId(line[1], Integer.parseInt(line[2]));
						budget.setBudgetAmount(Long.parseLong(line[3]));
						budgetRepository.save(budget);
						
						ProjectManager projectManager = new ProjectManager(line[4], line[0]);
						projectManagerRepository.save(projectManager);
						
						//line0 = ID Project code
						//line1 = Nama project
						//line2 = tahun
						//line3 = budget
						//line4 = ID PM
						
					}
				}
			} else if(file.getOriginalFilename().equals("user.csv")) {
				try(CSVReader reader = new CSVReader(new FileReader(convFile))){
					reader.readNext(); //skip column
					while ((line = reader.readNext()) != null) {
						lineNumber++;
						
						Users user = new Users();
						user.setUserId(line[0]);
						user.setUsername(line[1]);
						user.setPassword(line[2]);
						user.setRole(Integer.parseInt(line[3]));
						userRepository.save(user);
						
						//line0 = userid
						//line1 = username
						//line2 = password
						//line3 = role
					}
				}
			} else if(file.getOriginalFilename().equals("projectmanager.csv")) {
				try(CSVReader reader = new CSVReader(new FileReader(convFile))){
					reader.readNext(); //skip column
					while ((line = reader.readNext()) != null) {
						lineNumber++;
						
						ProjectManager projectManager = new ProjectManager(line[0], line[1]);
						projectManagerRepository.save(projectManager);
						
						//line0 = IdPegawai
						//line1 = ProjectCode
					}
				}
			} else if(file.getOriginalFilename().equals("departmenthead.csv")) {
				try(CSVReader reader = new CSVReader(new FileReader(convFile))){
					reader.readNext(); //skip column
					while ((line = reader.readNext()) != null) {
						lineNumber++;
						
						DepartmentHead departmentHead = new DepartmentHead();
						departmentHead.setDepartmentHeadId(line[0], line[1]);
						departmentHeadRepository.save(departmentHead);
						
						//line0 = IdPegawai
						//line1 = IdPM
					}
				}
			}
			
            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "ok";
    }

//    @GetMapping("/uploadStatus")
//    public String uploadStatus() {
//        return "uploadStatus";
//    }
    
    public String getCurrentTime(){
    	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    	LocalDateTime now = LocalDateTime.now();
    	System.out.println(dtf.format(now)); //2016/11/16 12:08:43
		return now.toString();
	}

}
