package com.bankmandiri.monitoring.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bankmandiri.monitoring.model.Budget;
import com.bankmandiri.monitoring.model.InternalOrder;
import com.bankmandiri.monitoring.model.NewProject;
import com.bankmandiri.monitoring.model.Pegawai;
import com.bankmandiri.monitoring.model.Pengadaan;
import com.bankmandiri.monitoring.model.Project;
import com.bankmandiri.monitoring.model.ProjectManager;
import com.bankmandiri.monitoring.model.Response;
import com.bankmandiri.monitoring.model.UploadPaid;
import com.bankmandiri.monitoring.model.UploadPlan;
import com.bankmandiri.monitoring.model.Vendor;
import com.bankmandiri.monitoring.repository.BudgetRepository;
import com.bankmandiri.monitoring.repository.InternalOrderRepository;
import com.bankmandiri.monitoring.repository.PegawaiRepository;
import com.bankmandiri.monitoring.repository.PengadaanRepository;
import com.bankmandiri.monitoring.repository.ProjectManagerRepository;
import com.bankmandiri.monitoring.repository.ProjectRepository;
import com.bankmandiri.monitoring.repository.UploadPaidRepository;
import com.bankmandiri.monitoring.repository.UploadPlanRepository;
import com.bankmandiri.monitoring.repository.VendorRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class InsertDataController {
	@Autowired
	VendorRepository vendorRepository;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	InternalOrderRepository internalOrderRepository;
	
	@Autowired
	PengadaanRepository pengadaanRepository;
	
	@Autowired
	PegawaiRepository PegawaiRepository;

	@RequestMapping(value = "/AddingNewVendor", method = RequestMethod.POST)
	@ResponseBody
	public Response InsertNewVendor(@RequestBody Vendor vendor) {
		Response response = new Response();
		
		try {
			vendorRepository.save(vendor);
			response.setFlag("1");
			response.setInfo("Success");
			response.setData(null);
			return response;
		} catch (Exception e) {
			response.setFlag("0");
			response.setInfo("Failed");
			response.setData(null);
			return response;
		}
	}
	
//	@RequestMapping(value = "/AddingNewProject", method = RequestMethod.POST)
//	@ResponseBody
//	public Response InsertNewProject(@RequestBody Project project) {
//		Response response = new Response();
//		try {
//			projectRepository.save(project);
//			response.setFlag("1");
//			response.setInfo("Success");
//			response.setData(null);
//			return response;
//		} catch (Exception e) {
//			response.setFlag("0");
//			response.setInfo("Failed");
//			response.setData(null);
//			return response;
//		}
//	}
	
	@RequestMapping(value = "/AddingNewIO", method = RequestMethod.POST)
	@ResponseBody
	public Response InsertNewIO(@RequestBody InternalOrder internalOrder) {
		Response response = new Response();
		try {
			internalOrderRepository.save(internalOrder);
			response.setFlag("1");
			response.setInfo("Success");
			response.setData(null);
			return response;
		} catch (Exception e) {
			response.setFlag("0");
			response.setInfo("Failed");
			response.setData(null);
			return response;
		}
	}
	
	@RequestMapping(value = "/AddingNewPegawai", method = RequestMethod.POST)
	@ResponseBody
	public Response InsertNewPegawai(@RequestBody Pegawai pegawai) {
		Response response = new Response();
		try {
			PegawaiRepository.save(pegawai);
			response.setFlag("1");
			response.setInfo("Success");
			response.setData(null);
			return response;
		} catch (Exception e) {
			response.setFlag("0");
			response.setInfo("Failed");
			response.setData(null);
			return response;
		}
	}
	
	public String getCurrentTime(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    	LocalDateTime now = LocalDateTime.now();
    	System.out.println(dtf.format(now)); //2016/11/16 12:08:43
		return now.toString();
	}
	
	@RequestMapping(value = "/AddingNewPengadaan", method = RequestMethod.POST)
	@ResponseBody
	public Response InsertPengadaan(@RequestBody Pengadaan pengadaan) {
		Response response = new Response();
		try {
			String idproject = pengadaan.getProjectCode();
			pengadaan.setNoId(getCurrentTime()+idproject);
			pengadaanRepository.save(pengadaan);
			response.setFlag("1");
			response.setInfo("Success");
			response.setData(null);
			return response;
		} catch (Exception e) {
			response.setFlag("0");
			response.setInfo("Failed");
			response.setData(null);
			return response;
		}
	}
	
	@Autowired
	ProjectManagerRepository projectManagerRepository;
	
	@RequestMapping(value = "/AddingNewPM", method = RequestMethod.POST)
	@ResponseBody
	public Response InsertPM(@RequestBody ProjectManager projectManager) {
		Response response = new Response();
		try {
			projectManagerRepository.save(projectManager);
			response.setFlag("1");
			response.setInfo("Success");
			response.setData(null);
			return response;
		} catch (Exception e) {
			response.setFlag("0");
			response.setInfo("Failed");
			response.setData(null);
			return response;
		}
	}
	
	private String parserObject(Object dataVendor) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		//Object to JSON in String
		return mapper.writeValueAsString(dataVendor);
	}
	
	@Autowired
	BudgetRepository budgetRepository;
	
	@RequestMapping(value = "/AddingNewProjectAndBudget", method = RequestMethod.POST)
	@ResponseBody
	public Response InsertProjectAndBudget(@RequestBody NewProject newProject) {
		Response response = new Response();
		try {
			Project project = new Project();
			project.setProjectCode(newProject.getProjectCode());
			project.setProjectName(newProject.getProjectName());
			projectRepository.save(project);
			
			Budget budget = new Budget();
			budget.setBudgetId(newProject.getProjectCode(), newProject.getTahun());
			budget.setBudgetAmount(newProject.getBudget());
			budgetRepository.save(budget);
			
			ProjectManager projectManager = new ProjectManager(newProject.getIdPM(), newProject.getProjectCode());;
			projectManagerRepository.save(projectManager);
			
			response.setFlag("1");
			response.setInfo("Success");
			response.setData(parserObject(newProject));
			return response;
			
		} catch (Exception e) {
			response.setFlag("0");
			response.setInfo("Failed");
			response.setData(e.toString());
			return response;
		}
	}
	
	@Autowired
	UploadPaidRepository uploadPaidRepository;
	
	@RequestMapping(value = "/AddUploadPaid", method = RequestMethod.POST)
	@ResponseBody
	public Response InsertUploadPaid(@RequestBody UploadPaid upload) {
		Response response = new Response();
		try {
			uploadPaidRepository.save(upload);
			response.setFlag("1");
			response.setInfo("Success");
			response.setData(null);
			return response;
		} catch (Exception e) {
			response.setFlag("0");
			response.setInfo("Failed");
			response.setData(null);
			return response;
		}
	}
	
	@Autowired
	UploadPlanRepository uploadPlanRepository;
	
	@RequestMapping(value = "/AddUploadPlan", method = RequestMethod.POST)
	@ResponseBody
	public Response InsertUpload(@RequestBody UploadPlan upload) {
		Response response = new Response();
		try {
			uploadPlanRepository.save(upload);
			response.setFlag("1");
			response.setInfo("Success");
			response.setData(null);
			return response;
		} catch (Exception e) {
			response.setFlag("0");
			response.setInfo("Failed");
			response.setData(null);
			return response;
		}
	}
}
