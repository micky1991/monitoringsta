package com.bankmandiri.monitoring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

import com.bankmandiri.monitoring.model.Pegawai;
import com.bankmandiri.monitoring.model.Response;
import com.bankmandiri.monitoring.model.Users;
import com.bankmandiri.monitoring.repository.PegawaiRepository;
import com.bankmandiri.monitoring.repository.UserRepository;


@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
    private UserRepository userRepository;
	@Autowired
    private PegawaiRepository pegawaiRepository;
    
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(UserRepository applicationUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping("/sign-up")
    public Response signUp(@RequestBody Users user) {
    	try {
    		Response response = new Response(); 
    		String userId = user.getUserId();
    		
        	Users getUser = (Users) userRepository.findByIdPegawai(userId);
        	
        	if (getUser == null) {
        		Pegawai pegawai = (Pegawai) pegawaiRepository.findPegawaiById(userId);
        		if(pegawai != null) {
        			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        			user.setRole(pegawai.getStatus());
        	        userRepository.save(user);
        	        response.setFlag("1");
        	        response.setInfo("userId Succes");
        	        return response;
        		} else {
        			response.setFlag("0");
        			response.setInfo("User not Found");
        			return response;
        		}
        	} else {
        		response.setFlag("0");
        		response.setInfo("User Has Account");
        		return response;
        	}
    	} catch (Exception e) {
    		Response response = new Response();
    		response.setFlag("0");
			response.setInfo(e.toString());
			return response;
    	}   
    }
    
    
}