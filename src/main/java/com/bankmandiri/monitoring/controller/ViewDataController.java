package com.bankmandiri.monitoring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bankmandiri.monitoring.model.DepartmentHead;
import com.bankmandiri.monitoring.model.InternalOrder;
import com.bankmandiri.monitoring.model.Pegawai;
import com.bankmandiri.monitoring.model.Pengadaan;
import com.bankmandiri.monitoring.model.Project;
import com.bankmandiri.monitoring.model.ProjectManager;
import com.bankmandiri.monitoring.model.Role;
import com.bankmandiri.monitoring.model.UploadPaid;
import com.bankmandiri.monitoring.model.UploadPlan;
import com.bankmandiri.monitoring.model.Vendor;
import com.bankmandiri.monitoring.repository.DepartmentHeadRepository;
import com.bankmandiri.monitoring.repository.InternalOrderRepository;
import com.bankmandiri.monitoring.repository.PegawaiRepository;
import com.bankmandiri.monitoring.repository.PengadaanRepository;
import com.bankmandiri.monitoring.repository.ProjectManagerRepository;
import com.bankmandiri.monitoring.repository.ProjectRepository;
import com.bankmandiri.monitoring.repository.RoleRepository;
import com.bankmandiri.monitoring.repository.UploadPaidRepository;
import com.bankmandiri.monitoring.repository.UploadPlanRepository;
import com.bankmandiri.monitoring.repository.VendorRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
public class ViewDataController {
	@Autowired
	VendorRepository vendorRepository;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	InternalOrderRepository internalOrderRepository;
	
	@Autowired
	PengadaanRepository pengadaanRepository;
	
	@Autowired
	ProjectManagerRepository projectManageRepository;
	
	@Autowired
	DepartmentHeadRepository departmentHeadRepository;
	
	@RequestMapping(value = "/getVendor")
	@ResponseBody
	public String GetVendor() {
		try {
			List<Vendor> dataVendor = (List<Vendor>) vendorRepository.findAll();
			
			if(dataVendor.size() > 0) {
				String json = parserObject(dataVendor);
				return json;
			} else {
				return "{\"response\" : {"
						+ "\"flag\" : 0,"
						+ "\"info\" : \"No Vendor found\"}}";
			}
		} catch (Exception e) {
			return "{\"response\" : \"" + e.toString() + "\"}";
		}
	}
	
	@RequestMapping(value = "/getVendorById", method = RequestMethod.POST)
	@ResponseBody
	public String GetVendorById(@RequestBody Vendor vendor) {
		try {
			Vendor dataVendor = (Vendor) vendorRepository.getOne(vendor.getIdVendor());
			
			if(dataVendor != null) {
				String json = parserObject(dataVendor);
				return json;
			} else {
				return "{\"response\" : {"
						+ "\"flag\" : 0,"
						+ "\"info\" : \"No Vendor found\"}}";
			}
		} catch (Exception e) {
			return "{\"response\" : \"" + e.toString() + "\"}";
		}
	}
	
	private String parserObject(Object dataVendor) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		//Object to JSON in String
		return mapper.writeValueAsString(dataVendor);
	}
	
//	@CrossOrigin(origins = "http://10.217.100.172:8080")
	@GetMapping(value = "/getProject")
	@ResponseBody
	public String GetProject() {
		try {
			List<Project> dataProject = (List<Project>) projectRepository.findAll();
			
			if(dataProject.size() > 0) {
				String json = parserObject(dataProject);
				return json;
			} else {
				return "{\"response\" : {"
						+ "\"flag\" : 0,"
						+ "\"info\" : \"No Project found\"}}";
			}
		} catch (Exception e) {
			return "{\"response\" : \"" + e.toString() + "\"}";
		}
	}
	
	@RequestMapping(value = "/getProjectByPM", method = RequestMethod.POST)
	@ResponseBody
	public String GetProjectByPM(@RequestBody Pegawai pegawai) {
		try {
			List<Project> dataProject = (List<Project>) projectRepository.findByPM(pegawai.getIdPegawai());
			
			if(dataProject.size() > 0) {
				String json = parserObject(dataProject);
				return json;
			} else {
				return "{\"response\" : {"
						+ "\"flag\" : 0,"
						+ "\"info\" : \"No Project found\"}}";
			}
		} catch (Exception e) {
			return "{\"response\" : \"" + e.toString() + "\"}";
		}
	}
	
	@RequestMapping(value = "/getPengadaanByProjectCode", method = RequestMethod.POST)
	@ResponseBody
	public String GetProjectByProjectCode(@RequestBody Project project) {
		try {
			List<Pengadaan> dataPengadaan = (List<Pengadaan>) pengadaanRepository.findByProjectCode(project.getProjectCode());
			return parserObject(dataPengadaan);
		} catch (Exception e) {
			return e.toString();
		}
		
	
	}
	
	@RequestMapping(value = "/getPMByDH", method = RequestMethod.POST)
	@ResponseBody
	public String GetPMByDH(@RequestBody DepartmentHead departmentHead) {
		try {
			List<ProjectManager> dataPengadaan = (List<ProjectManager>) projectManageRepository.findByDH(departmentHead.getDepartmentHeadId().getIdPegawai());
			return parserObject(dataPengadaan);
		} catch (Exception e) {
			return e.toString();
		}
		
	
	}
	@Autowired
	PegawaiRepository pegawaiRepository;
	
	@RequestMapping(value = "/getPM")
	@ResponseBody
	public String GetPM() {
		try {
		List<Pegawai> pegawai = (List<Pegawai>) pegawaiRepository.findPM(3);
			return parserObject(pegawai);
		} catch (Exception e) {
			return e.toString();
		}
	
	}
	
	@RequestMapping(value = "/getIOByProjectCode")
	@ResponseBody
	public String GetIObyProjectCode(@RequestBody Project project) {
		try {
		List<InternalOrder> io = (List<InternalOrder>) internalOrderRepository.findByProjectCode(project.getProjectCode());
			return parserObject(io);
		} catch (Exception e) {
			return e.toString();
		}
	
	}
	
	@Autowired
	RoleRepository roleRepository;
	
	@RequestMapping(value = "/getRole")
	@ResponseBody
	public String GetRole(@RequestBody Role role) {
		try {
		List<Role> roleList = (List<Role>) roleRepository.findAll();
			return parserObject(roleList);
		} catch (Exception e) {
			return e.toString();
		}
	
	}
	
	@RequestMapping(value = "/getPegawai")
	@ResponseBody
	public String GetPegawai() {
		try {
			List<Pegawai> pegawaiList = (List<Pegawai>) pegawaiRepository.findAll();
			return parserObject(pegawaiList);
		} catch (Exception e) {
			return e.toString();
		}
	
	}
	
	@RequestMapping(value = "/getPegawaiById", method = RequestMethod.POST)
	@ResponseBody
	public String GetPegawaiById(@RequestBody Pegawai pegawai) {
		try {
			Pegawai pegawaiList = (Pegawai) pegawaiRepository.findPegawaiById(pegawai.getIdPegawai());
			return parserObject(pegawaiList);
		} catch (Exception e) {
			return e.toString();
		}
	
	}
	
	@Autowired
	UploadPaidRepository uploadPaidRepository;
	
	@RequestMapping(value = "/getUploadPaidById", method = RequestMethod.POST)
	@ResponseBody
	public String GetUploadPaidById(@RequestBody UploadPaid uploadPaid) {
		try {
			List<UploadPaid> uploadPaidList = (List<UploadPaid>) uploadPaidRepository
					.findByIdPengadaan(uploadPaid.getIdPengadaan());
			return parserObject(uploadPaidList);
		} catch (Exception e) {
			return e.toString();
		}
	}
	
	@RequestMapping(value = "/getUploadPaidByIdAndTahun", method = RequestMethod.POST)
	@ResponseBody
	public String GetUploadPaidByIdAndTahun(@RequestBody UploadPaid uploadPaid) {
		try {
			List<UploadPaid> uploadPaidList = (List<UploadPaid>) uploadPaidRepository
					.findByIdPengadaanAndTahun(uploadPaid.getIdPengadaan(), 
							uploadPaid.getTahun());
			return parserObject(uploadPaidList);
		} catch (Exception e) {
			return e.toString();
		}
	}
	
	@RequestMapping(value = "/getUploadPaidByIdAndTahunAndQuartal", method = RequestMethod.POST)
	@ResponseBody
	public String GetUploadPaidByIdAndTahunAndQuartal(@RequestBody UploadPaid uploadPaid) {
		try {
			List<UploadPaid> uploadPaidList = (List<UploadPaid>) uploadPaidRepository
					.findByIdPengadaanAndTahunAndQuartal(uploadPaid.getIdPengadaan(), 
							uploadPaid.getTahun(), uploadPaid.getQuartal());
			return parserObject(uploadPaidList);
		} catch (Exception e) {
			return e.toString();
		}
	}
	
	@Autowired
	UploadPlanRepository uploadPlanRepository;
	
	@RequestMapping(value = "/getUploadPlanById", method = RequestMethod.POST)
	@ResponseBody
	public String GetUploadPlanById(@RequestBody UploadPlan uploadPlan) {
		try {
			List<UploadPlan> uploadPlanList = (List<UploadPlan>) uploadPlanRepository
					.findByIdPengadaan(uploadPlan.getIdPengadaan());
			return parserObject(uploadPlanList);
		} catch (Exception e) {
			return e.toString();
		}
	}
	
	@RequestMapping(value = "/getUploadPlanByIdAndTahun", method = RequestMethod.POST)
	@ResponseBody
	public String GetUploadPlanByIdAndTahun(@RequestBody UploadPlan uploadPlan) {
		try {
			List<UploadPlan> uploadPlanList = (List<UploadPlan>) uploadPlanRepository
					.findByIdPengadaanAndTahun(uploadPlan.getIdPengadaan(), 
							uploadPlan.getTahun());
			return parserObject(uploadPlanList);
		} catch (Exception e) {
			return e.toString();
		}
	}
	
	@RequestMapping(value = "/getUploadPlanByIdAndTahunAndQuartal", method = RequestMethod.POST)
	@ResponseBody
	public String GetUploadPlanByIdAndTahunAndQuartal(@RequestBody UploadPlan uploadPlan) {
		try {
			List<UploadPlan> uploadPlanList = (List<UploadPlan>) uploadPlanRepository
					.findByIdPengadaanAndTahunAndQuartal(uploadPlan.getIdPengadaan(), 
							uploadPlan.getTahun(), uploadPlan.getQuartal());
			return parserObject(uploadPlanList);
		} catch (Exception e) {
			return e.toString();
		}
	}
}
