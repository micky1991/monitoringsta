package com.bankmandiri.monitoring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bankmandiri.monitoring.model.Pengadaan;

public interface PengadaanRepository extends JpaRepository<Pengadaan, String>{
//	List<Pengadaan> findB(String idPengadaan);
	@Query(value = "SELECT * FROM pengadaan p WHERE p.project_code = :project_code", nativeQuery = true)
	List<Pengadaan> findByProjectCode(@Param("project_code") String project_code);
}
