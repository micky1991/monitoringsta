package com.bankmandiri.monitoring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bankmandiri.monitoring.model.DepartmentHead;

public interface DepartmentHeadRepository extends JpaRepository<DepartmentHead, String>{
	
}
