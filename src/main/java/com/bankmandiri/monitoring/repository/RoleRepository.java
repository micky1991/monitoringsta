package com.bankmandiri.monitoring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bankmandiri.monitoring.model.Role;

public interface RoleRepository extends JpaRepository<Role, String>{

}
