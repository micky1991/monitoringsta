package com.bankmandiri.monitoring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bankmandiri.monitoring.model.Project;

public interface ProjectRepository extends JpaRepository<Project, String>{
	@Query(value = "SELECT * FROM project_manager pm INNER JOIN project p ON p.project_code = pm.project_code WHERE pm.id_pegawai = :id_pegawai", nativeQuery = true)
	List<Project> findByPM(@Param("id_pegawai") String idPM);
	
	
}
