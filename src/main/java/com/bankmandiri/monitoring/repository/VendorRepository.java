package com.bankmandiri.monitoring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bankmandiri.monitoring.model.Vendor;

public interface VendorRepository extends JpaRepository<Vendor, String>{
//	List<Project> findB(String idProject);
}
