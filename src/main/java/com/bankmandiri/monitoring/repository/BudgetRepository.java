package com.bankmandiri.monitoring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bankmandiri.monitoring.model.Budget;

public interface BudgetRepository extends JpaRepository<Budget, String>{

}
