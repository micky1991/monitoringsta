package com.bankmandiri.monitoring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bankmandiri.monitoring.model.Users;

public interface UserRepository extends JpaRepository<Users, Long>{
	@Query(value = "SELECT * FROM users u WHERE u.username = :username", nativeQuery = true)
	Users findByUsername(@Param("username") String Username);

	@Query(value = "SELECT * FROM users u WHERE u.user_id = :idPegawai", nativeQuery = true)
	Users findByIdPegawai(@Param("idPegawai") String idPegawai);
}
