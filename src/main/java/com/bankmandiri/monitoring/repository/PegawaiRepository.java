package com.bankmandiri.monitoring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bankmandiri.monitoring.model.Pegawai;

public interface PegawaiRepository extends JpaRepository<Pegawai, String>{
	@Query(value = "SELECT * FROM pegawai p WHERE p.status = :status", nativeQuery = true)
	List<Pegawai> findPM(@Param("status") int Status);
	
	@Query(value = "SELECT * FROM pegawai p WHERE p.id_pegawai = :id_pegawai", nativeQuery = true)
	Pegawai findPegawaiById(@Param("id_pegawai") String UserId);
}