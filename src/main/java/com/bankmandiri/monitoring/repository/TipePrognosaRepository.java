package com.bankmandiri.monitoring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bankmandiri.monitoring.model.TipePrognosa;

public interface TipePrognosaRepository extends JpaRepository<TipePrognosa, String>{

}
