package com.bankmandiri.monitoring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bankmandiri.monitoring.model.InternalOrder;

public interface InternalOrderRepository extends JpaRepository<InternalOrder, String>{
	@Query(value = "SELECT * FROM internal_order WHERE project_code = :project_code", nativeQuery = true)
	List<InternalOrder> findByProjectCode(@Param("project_code") String projectCode);
}
