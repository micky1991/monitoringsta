package com.bankmandiri.monitoring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bankmandiri.monitoring.model.ProjectManager;

public interface ProjectManagerRepository extends JpaRepository<ProjectManager, String>{
	@Query(value = "SELECT * FROM pegawai p, department_head dh WHERE p.id_pegawai = dh.id_pm and = :id_pegawai", nativeQuery = true)
	List<ProjectManager> findByDH(@Param("id_pegawai") String IdPegawai);
}