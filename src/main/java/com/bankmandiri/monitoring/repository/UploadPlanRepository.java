package com.bankmandiri.monitoring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bankmandiri.monitoring.model.UploadPlan;

public interface UploadPlanRepository extends JpaRepository<UploadPlan, String>{
	@Query(value = "SELECT * FROM upload_plan up WHERE up.id_pengadaan = :id_pengadaan", nativeQuery = true)
	List<UploadPlan> findByIdPengadaan(@Param("id_pengadaan") String IdPengadaan);
	
	@Query(value = "SELECT * FROM upload_plan up WHERE up.id_pengadaan = :id_pengadaan AND up.tahun = :tahun", nativeQuery = true)
	List<UploadPlan> findByIdPengadaanAndTahun(@Param("id_pengadaan") String IdPengadaan, @Param("tahun") int tahun);
	
	@Query(value = "SELECT * FROM upload_plan up WHERE up.id_pengadaan = :id_pengadaan AND up.tahun = :tahun AND up.quartal = :quartal", nativeQuery = true)
	List<UploadPlan> findByIdPengadaanAndTahunAndQuartal(@Param("id_pengadaan") String IdPengadaan, @Param("tahun") int tahun, @Param("quartal") int quartal);
}
