package com.bankmandiri.monitoring.security;

public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 600_000; // 1 minute
//    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String LOGIN_URL = "/login";
    public static final String VENDOR_URL = "/AddingNewVendor";
    public static final String PEGAWAI_URL = "/getPegawai";
    public static final String VENDOR_GET_ID_URL = "/getVendorById";
    public static final String VENDOR_GET_URL = "/getVendor";
    public static final String UPLOAD_URL = "/";
    public static final String UPLOAD_RESULT_URL = "/upload";
    public static final String UPLOAD_PLAN_URL = "/AddUploadPlan";
    public static final String UPLOAD_PAID_URL = "/AddUploadPaid";
    public static final String GET_UPLOAD_PLAN_BY_ID_URL = "/getUploadPlanById";
    public static final String GET_UPLOAD_PAID_BY_ID_URL = "/getUploadPaidById";
    public static final String GET_UPLOAD_PLAN_BY_ID_AND_TAHUN_URL = "/getUploadPlanByIdAndTahun";
    public static final String GET_UPLOAD_PAID_BY_ID_AND_TAHUN_URL = "/getUploadPaidByIdAndTahun";
    public static final String GET_UPLOAD_PLAN_BY_ID_AND_TAHUN_AND_QUARTAL_URL = "/getUploadPlanByIdAndTahunAndQuartal";
    public static final String GET_UPLOAD_PAID_BY_ID_AND_TAHUN_AND_QUARTAL_URL = "/getUploadPaidByIdAndTahunAndQuartal";
    public static final String GET_PROJECT_URL = "/getProjectByPM";
    public static final String GET_PENGADAAN_URL = "/getPengadaanByProjectCode";
}
