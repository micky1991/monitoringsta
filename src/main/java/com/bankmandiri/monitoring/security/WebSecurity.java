package com.bankmandiri.monitoring.security;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import static com.bankmandiri.monitoring.security.SecurityConstants.SIGN_UP_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.LOGIN_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.PEGAWAI_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.VENDOR_GET_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.VENDOR_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.UPLOAD_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.UPLOAD_RESULT_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.VENDOR_GET_ID_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.UPLOAD_PLAN_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.UPLOAD_PAID_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.GET_UPLOAD_PLAN_BY_ID_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.GET_UPLOAD_PAID_BY_ID_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.GET_UPLOAD_PLAN_BY_ID_AND_TAHUN_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.GET_UPLOAD_PAID_BY_ID_AND_TAHUN_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.GET_UPLOAD_PLAN_BY_ID_AND_TAHUN_AND_QUARTAL_URL;
import static com.bankmandiri.monitoring.security.SecurityConstants.GET_UPLOAD_PAID_BY_ID_AND_TAHUN_AND_QUARTAL_URL;

import com.bankmandiri.monitoring.security.UserDetailsServiceImpl;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter{
	private UserDetailsServiceImpl userDetailsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public WebSecurity(UserDetailsServiceImpl userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().authorizeRequests()
        	.antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
        	.antMatchers(HttpMethod.POST, LOGIN_URL).permitAll()
        	.antMatchers(HttpMethod.POST, PEGAWAI_URL).permitAll()
        	.antMatchers(HttpMethod.POST, VENDOR_GET_URL).permitAll()
        	.antMatchers(HttpMethod.POST, VENDOR_URL).permitAll()
        	.antMatchers(HttpMethod.POST, UPLOAD_URL).permitAll()
        	.antMatchers(HttpMethod.POST, UPLOAD_RESULT_URL).permitAll()
        	.antMatchers(HttpMethod.POST, VENDOR_GET_ID_URL).permitAll()
        	.antMatchers(HttpMethod.POST, UPLOAD_PLAN_URL).permitAll()
        	.antMatchers(HttpMethod.POST, UPLOAD_PAID_URL).permitAll()
        	.antMatchers(HttpMethod.POST, GET_UPLOAD_PLAN_BY_ID_URL).permitAll()
        	.antMatchers(HttpMethod.POST, GET_UPLOAD_PAID_BY_ID_URL).permitAll()
        	.antMatchers(HttpMethod.POST, GET_UPLOAD_PLAN_BY_ID_AND_TAHUN_URL).permitAll()
        	.antMatchers(HttpMethod.POST, GET_UPLOAD_PAID_BY_ID_AND_TAHUN_URL).permitAll()
        	.antMatchers(HttpMethod.POST, GET_UPLOAD_PLAN_BY_ID_AND_TAHUN_AND_QUARTAL_URL).permitAll()
        	.antMatchers(HttpMethod.POST, GET_UPLOAD_PAID_BY_ID_AND_TAHUN_AND_QUARTAL_URL).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
                .addFilter(new JWTAuthorizationFilter(authenticationManager()))
                // this disables session creation on Spring Security
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
    	final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    	source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
    	return source;
    }
}
