package com.bankmandiri.monitoring.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.security.core.userdetails.User;

import com.bankmandiri.monitoring.model.Users;
import com.bankmandiri.monitoring.repository.UserRepository;

import static java.util.Collections.emptyList;

@Component
public class UserDetailsServiceImpl implements UserDetailsService{
	private UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository applicationUserRepository) {
        this.userRepository = applicationUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = userRepository.findByUsername(username);
        if (users == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(users.getUsername(), users.getPassword(), emptyList());
    }
}
